<?php

/*-----------------------------------------
		SIDEBARS - www.wp-hasty.com
-----------------------------------------*/
function news_sidebar() {
	$args = array(
		'name'          => __( 'Blog Sidebar' ),
		'id'            => 'sidebar-blog',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'news_sidebar' );