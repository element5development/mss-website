<?php

/*-----------------------------------------
  CORRECT TAB INDEX ON FORMS
-----------------------------------------*/
add_filter("gform_tabindex", create_function("", "return false;"));

/*-----------------------------------------
  EABLE LABEL VISIBILITY OPTION
-----------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*-----------------------------------------
  SUBMIT INPUT TO BUTTON ELEMENT
-----------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="is-secondary"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );

/*-----------------------------------------
  PHYSICIAN DROP DOWN DYNAMIC
-----------------------------------------*/
add_filter( 'gform_pre_render_4', 'populate_posts' );
add_filter( 'gform_pre_validation_4', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_4', 'populate_posts' );
add_filter( 'gform_admin_pre_render_4', 'populate_posts' );
function populate_posts( $form ) {
	foreach ( $form['fields'] as &$field ) {
		if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
				continue;
		}
		// you can add additional parameters here to alter the posts that are retrieved
		// more info: http://codex.wordpress.org/Template_Tags/get_posts
		$posts = get_posts(array(
			'numberposts' => -1,
			'post_type' => 'staff',
			'tax_query' => array(
				array(
					'taxonomy' => 'position',
					'field' => 'slug',
					'terms' => array('hand-surgery', 'orthopedics'),
					'include_children' => false,
					'operator' => 'IN'
				)
			)
		));
		//&tax_query=array(array(taxonomy=position&field=id&terms=array(13,14)))
		$choices = array();
		foreach ( $posts as $post ) {
				$choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
		}
		// update 'Select a Post' to whatever you'd like the instructive option to be
		$field->placeholder = 'Requested Physician';
		$field->choices = $choices;
}
	return $form;
}