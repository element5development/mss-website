<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/
// Post Type Key: staff
function create_staff_cpt() {
	$labels = array(
		'name' => __( 'Staff', 'Post Type General Name' ),
		'singular_name' => __( 'Staff', 'Post Type Singular Name' ),
		'menu_name' => __( 'Staff' ),
		'name_admin_bar' => __( 'Staff' ),
		'archives' => __( 'Staff Archives' ),
		'attributes' => __( 'Staff Attributes' ),
		'parent_item_colon' => __( 'Parent Staff:' ),
		'all_items' => __( 'All Staff' ),
		'add_new_item' => __( 'Add New Staff' ),
		'add_new' => __( 'Add New' ),
		'new_item' => __( 'New Staff' ),
		'edit_item' => __( 'Edit Staff' ),
		'update_item' => __( 'Update Staff' ),
		'view_item' => __( 'View Staff' ),
		'view_items' => __( 'View Staff' ),
		'search_items' => __( 'Search Staff' ),
		'not_found' => __( 'Not found' ),
		'not_found_in_trash' => __( 'Not found in Trash' ),
		'featured_image' => __( 'Featured Image' ),
		'set_featured_image' => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image' => __( 'Use as featured image' ),
		'insert_into_item' => __( 'Insert into Staff' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Staff' ),
		'items_list' => __( 'Staff list' ),
		'items_list_navigation' => __( 'Staff list navigation' ),
		'filter_items_list' => __( 'Filter Staff list' ),
	);
	$args = array(
		'label' => __( 'Staff' ),
		'description' => __( '' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-groups',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'taxonomies' => array('position', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'staff', $args );
}
add_action( 'init', 'create_staff_cpt', 0 );
// Post Type Key: location
function create_location_cpt() {
	$labels = array(
		'name' => __( 'Locations', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Location', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Locations', 'textdomain' ),
		'name_admin_bar' => __( 'Location', 'textdomain' ),
		'archives' => __( 'Location Archives', 'textdomain' ),
		'attributes' => __( 'Location Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
		'all_items' => __( 'All Locations', 'textdomain' ),
		'add_new_item' => __( 'Add New Location', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Location', 'textdomain' ),
		'edit_item' => __( 'Edit Location', 'textdomain' ),
		'update_item' => __( 'Update Location', 'textdomain' ),
		'view_item' => __( 'View Location', 'textdomain' ),
		'view_items' => __( 'View Locations', 'textdomain' ),
		'search_items' => __( 'Search Location', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Location', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Location', 'textdomain' ),
		'items_list' => __( 'Locations list', 'textdomain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Locations list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Location', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'location', $args );
}
add_action( 'init', 'create_location_cpt', 0 );

// Pull in all locations in alphebetical order for archive
function show_all_locations( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {
		if ( is_post_type_archive( 'location' ) ) {
			$query->set('posts_per_page', -1 );
			$query->set( 'orderby', 'title' );
			$query->set( 'order', 'ASC' );
		}
	}
}
add_action( 'pre_get_posts', 'show_all_locations' );

// Post Type Key: insurance
function create_insurance_cpt() {

	$labels = array(
		'name' => __( ' Insurances', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( ' Insurance', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( ' Insurances', 'textdomain' ),
		'name_admin_bar' => __( ' Insurance', 'textdomain' ),
		'archives' => __( ' Insurance Archives', 'textdomain' ),
		'attributes' => __( ' Insurance Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent  Insurance:', 'textdomain' ),
		'all_items' => __( 'All  Insurances', 'textdomain' ),
		'add_new_item' => __( 'Add New  Insurance', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New  Insurance', 'textdomain' ),
		'edit_item' => __( 'Edit  Insurance', 'textdomain' ),
		'update_item' => __( 'Update  Insurance', 'textdomain' ),
		'view_item' => __( 'View  Insurance', 'textdomain' ),
		'view_items' => __( 'View  Insurances', 'textdomain' ),
		'search_items' => __( 'Search  Insurance', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into  Insurance', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this  Insurance', 'textdomain' ),
		'items_list' => __( ' Insurances list', 'textdomain' ),
		'items_list_navigation' => __( ' Insurances list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter  Insurances list', 'textdomain' ),
	);
	$args = array(
		'label' => __( ' Insurance', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-clipboard',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'insurance', $args );
}
add_action( 'init', 'create_insurance_cpt', 0 );