<?php

/*-----------------------------------------
	CUSTOM TAXONOMIES - www.wp-hasty.com
-----------------------------------------*/
// Taxonomy Key: position
function create_position_tax() {

	$labels = array(
		'name'              => _x( 'Positions', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Position', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Positions', 'textdomain' ),
		'all_items'         => __( 'All Positions', 'textdomain' ),
		'parent_item'       => __( 'Parent Position', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Position:', 'textdomain' ),
		'edit_item'         => __( 'Edit Position', 'textdomain' ),
		'update_item'       => __( 'Update Position', 'textdomain' ),
		'add_new_item'      => __( 'Add New Position', 'textdomain' ),
		'new_item_name'     => __( 'New Position Name', 'textdomain' ),
		'menu_name'         => __( 'Position', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'position', array('staff', 'physician' ), $args );

}
add_action( 'init', 'create_position_tax' );

// Taxonomy Key: specialty
function create_specialty_tax() {
	$labels = array(
		'name'              => _x( 'Specialties', 'taxonomy general name' ),
		'singular_name'     => _x( 'Specialty', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Specialties' ),
		'all_items'         => __( 'All Specialties' ),
		'parent_item'       => __( 'Parent Specialty' ),
		'parent_item_colon' => __( 'Parent Specialty:' ),
		'edit_item'         => __( 'Edit Specialty' ),
		'update_item'       => __( 'Update Specialty' ),
		'add_new_item'      => __( 'Add New Specialty' ),
		'new_item_name'     => __( 'New Specialty Name' ),
		'menu_name'         => __( 'Specialty' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'specialty', array('staff', ), $args );
}
add_action( 'init', 'create_specialty_tax' );