<?php

/*-----------------------------------------
  ENABLE ACF OPTIONS PAGE
-----------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

/*-----------------------------------------
  GOOGLE MAP
-----------------------------------------*/
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyBxlx86WdZSESQTjGusB9YEM3p5rZSAgPU');
}
add_action('acf/init', 'my_acf_init');
/*-----------------------------------------
		INCLUDE ACF INTO RELEVANSSI EXCERPTS
-----------------------------------------*/
add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
function custom_fields_to_excerpts($content, $post, $query) {

		/*
			Repeat the below code for each acf element you want to appear
			in the excerpt. Place them in the order you would want them
			to appear in the excerpt.

			There is no current way to include the repeater or flexiable
			content acf elements, keep this in mind when building pages.
		*/

		$custom_field = get_post_meta($post->ID, 'page_title', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'page_description', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'post_title', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'post_description', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'thank_you_message', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'city', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'biography', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'education', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'col_heading', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'column_left', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'column_right', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'content_continued', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'cta_heading', true);
		$content .= " " . $custom_field;

		$custom_field = get_post_meta($post->ID, 'cta_description', true);
		$content .= " " . $custom_field;
		return $content;

}
?>