<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>
	<article>
		<?php if( get_field('intro_content') ) { ?>
			<?php get_template_part('template-parts/pages/intro'); ?>
		<?php } ?>
		<?php if( !empty(get_the_content()) ) { ?>
			<div class="default-contents">
				<?php the_content(); ?>
			</div>
		<?php } ?>
		<?php if( get_field('col_heading') ) { ?>
			<div class="two_col default-contents">
				<h2><?php the_field('col_heading'); ?></h2>
				<div class="block">
					<?php the_field('column_left'); ?>
				</div>
				<div class="block">
					<?php the_field('column_right'); ?>
				</div>
			</div>
		<?php } ?>
		<?php if( get_field('cta_button') ) { ?>
			<?php get_template_part('template-parts/elements/basic-cta'); ?>
		<?php } ?>
		<?php if( get_field('content_continued') ) { ?>
			<div class="default-contents">
				<?php the_field('content_continued'); ?>
			</div>
		<?php } ?>
	</article>
</main>

<?php get_footer(); ?>