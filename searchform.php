<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<form role="search" method="get" action="<?php echo get_site_url(); ?>">
  <label>Search</label>
  <input type="search" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
  <button type="submit" class="is-secondary">
		<svg viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
			<path data-name="layer1" d="M58.121 52.879L42.545 37.302l-.29.308a20.009 20.009 0 1 0-3.777 3.995l-.057.06L53.88 57.12a3 3 0 0 0 4.242-4.242zM26 41a15 15 0 1 1 15-15 15.017 15.017 0 0 1-15 15z" />
		</svg>
	</button>
</form>