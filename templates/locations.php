<?php 
/*-------------------------------------------------------------------
    Template Name: Locations
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>
	<article>
		<?php if( !empty(get_the_content()) ) { ?>
			<div class="default-contents">
				<?php the_content(); ?>
			</div>
		<?php } ?>
		<?php
			$args = array(
				'post_type'      => 'location',
				'orderby' 			 => 'title',
				'order' 				 => 'ASC',
				'posts_per_page' => -1,
			);
			$locations = new WP_Query( $args );
		?>
		<?php if ( $locations->have_posts() ) : ?>
			<div class="default-contents">
				<div class="acf-map">
				<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
						<?php $location = get_field('map'); ?>
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							<h4><?php the_field('city'); ?></h4>
							<p class="address"><?php the_field('address'); ?> <?php the_field('address_line_2'); ?></p>
							<?php $mapLink = 'https://maps.google.com/?q=' . get_field('address') . get_field('address_line_2') . get_field('city') . get_field('state') . get_field('zip'); ?>
							<a target="_blank" href="<?php echo $mapLink ?>">view on google maps</a>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="locations-container">
					<?php while ( $locations->have_posts() ) : $locations->the_post(); ?>
						<div class="single-location">
							<h2><?php the_field('city'); ?></h2>
							<p><?php the_field('services'); ?></p>
							<?php $phone = preg_replace( '/[^0-9]/', '', get_field('phone') ); ?>
							<address>
								<?php the_field('address'); ?><br/>
								<?php the_field('address_line_2'); ?><br/>
								<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/>
								Phone: <a href="tel:+1<?php echo $phone; ?>"><?php the_field('phone'); ?></a><br/>
							</address>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		<?php endif; wp_reset_postdata(); ?>

	</article>
</main>

<?php get_footer(); ?>