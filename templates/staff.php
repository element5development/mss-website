<?php 
/*-------------------------------------------------------------------
    Template Name: Staff
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>
	<?php if( !empty(get_the_content()) ) { ?>
		<div class="default-contents">
			<?php the_content(); ?>
		</div>
	<?php } ?>
	<section class="staff-feed feed default-contents">
		<nav class="cat-nav">
			<?php 
				$terms = get_terms( array(
					'taxonomy' => 'position',
					'hide_empty' => false,
				) );
			?>
			<button class="is-active" data-filter="all">All</button>
			<?php foreach ($terms as $term) { ?>
				<button data-filter="<?php echo $term->slug;?>">
					<?php echo $term->name; ?>
				</button>
			<?php } ?>  
		</nav>
		<?php foreach ($terms as $term) { ?>
			<?php //QUERY STAFF
				$args = array( 
					'posts_per_page'  => -1, 
					'post_type' => 'staff',
					'orderby'=>'title',
					'order'=>'ASC',
					'tax_query' => array(
						array(
								'taxonomy' => 'position',
								'field' => 'slug',
								'terms' => array ( $term->slug )
						)
					)
				);
				$staff_query = new WP_Query( $args );
			?>
			<?php if ( $staff_query->have_posts() ) : ?>
				<div class="<?php echo  $term->slug; ?> staff-container">
					<h2><?php echo $term->name; ?></h2>
					<?php while ( $staff_query->have_posts() ) : $staff_query->the_post(); ?>
						<?php get_template_part('template-parts/posts/previews/preview-staff'); ?>
					<?php endwhile; ?>
				</div>
			<?php endif; wp_reset_postdata(); ?>
		<?php } ?>  

	</section>
	<?php if( get_field('cta_button') ) { ?>
		<?php get_template_part('template-parts/elements/basic-cta'); ?>
	<?php } ?>
</main>

<?php get_footer(); ?>