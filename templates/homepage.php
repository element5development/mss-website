<?php 
/*-------------------------------------------------------------------
    Template Name: Homepage
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>

	<?php 
		$args = array(
			'post_type'      => 'page',
			'posts_per_page' => -1,
			'post_parent'    => 168,
			'post__not_in' => array(174),
			'order'          => 'ASC',
			'orderby'        => 'menu_order'
		);
		$parent = new WP_Query( $args );
	?>
	<?php if ( $parent->have_posts() ) { ?>
		<div class="child-block">
			<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				<?php get_template_part('template-parts/pages/child-preview'); ?>
			<?php endwhile; ?>
		</div>
	<?php } wp_reset_postdata(); ?>

	<?php if( have_rows('actions') ): ?>
		<section class="actions">
			<?php while ( have_rows('actions') ) : the_row(); ?>
				<div class="action">
					<h3><?php the_sub_field('title'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
					<?php $button = get_sub_field('button'); ?>
					<a class="button is-secondary" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
				</div>
			<?php endwhile; ?>
		</section>
	<?php endif; ?>

	<?php if( !empty(get_the_content()) ) { ?>
		<article class="default-contents">
			<?php the_content(); ?>
		</article>
	<?php } ?>

	<?php if( get_field('cta_button') ) { ?>
		<?php get_template_part('template-parts/elements/basic-cta'); ?>
	<?php } ?>

	<?php 
		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => 2,
		);
		$news = new WP_Query( $args );
	?>
	<?php if ( $news->have_posts() ) { ?>
		<section class="home-feed feed default-contents">
			<h2>News & Events</h2>
			<?php while ( $news->have_posts() ) : $news->the_post(); ?>
				<?php get_template_part('template-parts/posts/previews/preview-post'); ?>
			<?php endwhile; ?>
		</section>
	<?php } wp_reset_postdata(); ?>

</main>

<?php get_footer(); ?>