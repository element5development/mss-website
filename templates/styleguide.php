<?php 
/*-------------------------------------------------------------------
    Template Name: Styleguide
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<a id="content" class="anchor"></a>

<main class="styleguide-container">
	<div class="brand block">
		<!--  COLORS  -->
		<section class="styleguide-section">
			<h2>Colors palette</h2>
			<div class="colors">
				<p>Primary Colors</p>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
			</div>
			<div class="colors">
				<p>NEUTRALS</p>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<a href="javascript:void(0);" class="color-swatch click-to-copy" data-clipboard-text="error">
						<p>COPIED!</p>
					</a>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
			</div>
		</section>
		<!--  FONTS -->
		<section class="styleguide-section">
			<h2>FONTS</h2>
			<div class="fonts">
				<div class="font">
					<div class="block">
						<p class="family"></p>
						<p class="example">Aa</p>
						<p class="weight"></p>
					</div>
				</div>
				<div class="font">
					<div class="block">
						<p class="family"></p>
						<p class="example">Aa</p>
						<p class="weight"></p>
					</div>
				</div>
			</div>
		</section>
		<!--  TYPOGRAPHY -->
		<section class="styleguide-section">
			<h2>TYPOGRAPHY</h2>
			<div class="headings">
				<div class="typography">
					<p>H1</p>
					<h1>This is a headline</h1>
				</div>
				<div class="typography">
					<p>H2</p>
					<h2>This is a headline</h2>
				</div>
				<div class="typography">
					<p>H3</p>
					<h3>This is a headline</h3>
				</div>
				<div class="typography">
					<p>H4</p>
					<h4>This is a headline</h4>
				</div>
				<div class="typography">
					<p>H5</p>
					<h5>This is a headline</h5>
				</div>
				<div class="typography">
					<p>Paragraph</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet pharetra metus. Sed a purus massa. Cras tempus pharetra quam, nec mattis dui semper et. Nunc sapien arcu, bibendum a euismod eget, facilisis quis nisl.</p>
				</div>
			</div>
		</section>
		<!--  LOGOS  -->
		<section class="styleguide-section">
			<h2>Logo</h2>
			<div class="logos">
				<div class="logo">
					<?php $logo_primary = get_field('primary_logo'); ?>
					<a target="_blank" href="<?php echo $logo_primary['url']; ?>">
						<img src="<?php echo $logo_primary['url']; ?>" alt="<?php echo $logo_primary['alt']; ?>" />
					</a>
				</div>
				<?php if( have_rows('logo_variant') ):
					while ( have_rows('logo_variant') ) : the_row(); ?>
						<div class="logo" style="background-color: <?php the_sub_field('background_color'); ?>">
						<?php $logo_variant = get_sub_field('logo'); ?>
						<a target="_blank" href="<?php echo $logo_variant['url']; ?>">
							<img src="<?php echo $logo_variant['url']; ?>" alt="<?php echo $logo_variant['alt']; ?>" />
						</a>
						</div>
					<?php endwhile;
				endif; ?>
			</div>
		</section>
		<!--  ICONS -->
		<?php if( have_rows('icons') ): ?>
			<section class="styleguide-section">
				<h2>ICONS</h2>
				<div class="icons">
					<?php while ( have_rows('icons') ) : the_row(); ?>
						<div class="icon">
							<?php $icon = get_sub_field('icon'); ?>
							<a target="_blank" href="<?php echo $icon['url']; ?>">
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<!--  IMAGERY -->
		<?php if( have_rows('imagery') ): ?>
			<section class="styleguide-section">
				<h2>IMAGERY</h2>
				<div class="images">
					<?php while ( have_rows('imagery') ) : the_row(); ?>
						<?php $image = get_sub_field('image'); ?>
						<a target="_blank" href="<?php echo $image['url']; ?>" class="image" style="background-image: url('<?php echo $image['url']; ?>');"></a>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
	</div>
	<div class="ui-elements block">
		<!--  BUTTONS -->
		<section class="styleguide-section">
			<h2>Buttons</h2>
			<div class="buttons">
				<p>Primary</p>
				<a href="" class="button">button</a>
				<a href="" class="button is-hover">hover</a>
				<a href="" class="button is-active">active</a>
				<pre><code>[btn target="_blank" url="https://www.google.com/" type"primary" size="normal"]Short Code Button[/btn]</code></pre>
			</div>
			<div class="buttons">
				<p>Secondary</p>
				<a href="" class="button is-secondary">button</a>
				<a href="" class="button is-secondary is-hover">hover</a>
				<a href="" class="button is-secondary is-active">active</a>
				<pre><code>[btn target="_blank" url="https://www.google.com/" type"secondary" size="normal"]Short Code Button[/btn]</code></pre>
			</div>
			<div class="buttons">
				<p>Small Buttons</p>
				<a href="" class="button is-small">small button</a>
				<a href="" class="button is-small is-hover">small hover</a>
				<a href="" class="button is-small is-active">small active</a><br/>
				<a href="" class="button is-secondary is-small">small button</a>
				<a href="" class="button is-secondary is-small is-hover">small hover</a>
				<a href="" class="button is-secondary is-small is-active">small active</a>
<pre><code>[btn target="_blank" url="https://www.google.com/" type"secondary" size="small"]Short Code Button[/btn]
[btn target="_blank" url="https://www.google.com/" type"primary" size="small"]Short Code Button[/btn]
</code></pre>
			</div>
			<div class="buttons">
				<p>Tweet This</p>
				<a class="tweet-this" target="_blank" href="http://twitter.com/home?status=Curabitur tristique dolor id ipsum condimentum ornare. via @element5">Curabitur tristique dolor id ipsum condimentum ornare.</a>
				<span class="twitter-share">
					<svg viewBox="0 0 24 24">
						<path d="M23.954 4.57c-.885.388-1.83.653-2.825.774 1.013-.61 1.793-1.574 2.162-2.723-.95.556-2.005.96-3.127 1.185-.896-.96-2.173-1.56-3.59-1.56-2.718 0-4.92 2.204-4.92 4.918 0 .39.044.765.126 1.124C7.69 8.094 4.067 6.13 1.64 3.16c-.427.723-.666 1.562-.666 2.476 0 1.71.87 3.213 2.188 4.096-.807-.026-1.566-.248-2.228-.616v.06c0 2.386 1.693 4.375 3.946 4.828-.413.11-.85.17-1.296.17-.314 0-.615-.03-.916-.085.63 1.952 2.445 3.376 4.604 3.416-1.68 1.32-3.81 2.105-6.102 2.105-.39 0-.78-.022-1.17-.066 2.19 1.394 4.768 2.21 7.557 2.21 9.054 0 14-7.497 14-13.987 0-.21 0-.42-.016-.63.962-.69 1.8-1.56 2.46-2.548l-.046-.02z"/>
					</svg>
				</span>
				<pre><code>[tweet username="@username"]Content to tweet[/tweet]</code></pre>
			</div>
			<div class="buttons">
				<p>Social Links</p>
				<a target="_blank" href="https://www.facebook.com/" class="social-button facebook">
					<svg aria-label="Facebook" role="img" viewBox="0 0 512 512">
						<rect width="512" height="512" fill="#3b5998" rx="15%"/>
						<path fill="#fff" d="M330 512V322h64l9-74h-73v-47c0-22 6-36 37-36h39V99c-7-1-30-3-57-3-57 0-95 34-95 98v54h-64v74h64v190z"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/" class="social-button linkedin">
					<svg fill="#fff" aria-label="LinkedIn" role="img" viewBox="0 0 512 512">
						<rect width="512" height="512" fill="#0077b5" rx="15%"/>
						<circle cx="142" cy="138" r="37"/>
						<path stroke="#fff" stroke-width="66" d="M244 194v198M142 194v198"/>
						<path d="M276 282c0-20 13-40 36-40 24 0 33 18 33 45v105h66V279c0-61-32-89-76-89-34 0-51 19-59 32"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.youtube.com/" class="social-button youtube">
					<svg fill="#ed1d24" viewBox="0 0 512 512" aria-label="YouTube" role="img">
						<rect height="512" rx="15%" width="512"/>
						<path d="m427 169c-4-15-17-27-32-31-34-9-239-10-278 0-15 4-28 16-32 31-9 38-10 135 0 174 4 15 17 27 32 31 36 10 241 10 278 0 15-4 28-16 32-31 9-36 9-137 0-174" fill="#fff"/>
						<path d="m220 203v106l93-53"/>
					</svg>
				</a>
			</div>
		</section>
		<!-- INPUT FIELDS -->
		<section class="styleguide-section">
			<h2>INPUT FIELDS</h2>
			<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
		</section>
		<!-- LISTS -->
		<section class="styleguide-section">
			<h2>Lists</h2>
			<div class="lists">
				<div class="list">
					<ul>
						<li>Unordered List</li>
						<li>Unordered List</li>
						<li>Unordered List</li>
					</ul>
				</div>
				<div class="list">
					<ol>
						<li>Unordered List</li>
						<li>Unordered List</li>
						<li>Unordered List</li>
					</ol>
				</div>
			</div>
		</section>
		<!-- BLOCKQUOTE -->
		<section class="styleguide-section">
			<h2>Blockquote</h2>
			<div class="quotes">
				<blockquote>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu condimentum ex, non tincidunt enim. 
					<span>Quotee</span>
				</blockquote>
			</div>
		</section>
	</div>
</main>

<?php get_footer(); ?>