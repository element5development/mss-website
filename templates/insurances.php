<?php 
/*-------------------------------------------------------------------
		Template Name: Insurances
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>
	<article>
		<?php if( !empty(get_the_content()) ) { ?>
			<div class="default-contents">
				<?php the_content(); ?>
			</div>
		<?php } ?>
		<section class="insurance-feed feed default-contents">
			<a href="#content" class="back-to-top button smooth-scroll">back to top</a>
			<?php $alphas = range('A', 'Z'); ?>
			<nav class="letter-nav">
				<?php foreach($alphas as $letter) { ?>
					<a href="#<?php echo $letter; ?>" class="smooth-scroll"><?php echo $letter; ?></a>
				<?php } ?>
			</nav>
			<?php //QUERY ALL insurance
				$args = array( 
					'posts_per_page'  => -1, 
					'post_type' => 'insurance',
					'orderby'=>'title',
					'order'=>'ASC'
					);
				$insurance_query = new WP_Query( $args );
			?>
			<?php if ( $insurance_query->have_posts() ) : ?>
				<?php 
					$last_post_letter = 'Z';
					$this_post_letter = 'A';
					$alphebet = range('A', 'Z');
				?>
				<?php while ( $insurance_query->have_posts() ) : $insurance_query->the_post(); ?>
					<?php	$this_post_letter = strtoupper( substr( $post->post_title, 0, 1 ) ); ?>
					<?php if ( $this_post_letter == $last_post_letter ) { ?>
						<!-- Nothing -->
					<?php } else { ?>
							<?php
								$last_post_value = array_search($last_post_letter, $alphebet);
								$this_post_value = array_search($this_post_letter, $alphebet);
								$differance = $this_post_value - $last_post_value;
								if ( $differance < 0 ) {
									echo '<h2>A</h2>';
									echo '<a id="A" class="letter-anchor"></a><hr>';
								} else {
									$i = 1; 
									while( $i <= $differance ) {
										$position = $last_post_value + $i;
										echo '<h2>' . $alphebet[$position] . '</h2>';
										echo '<a id="' . $alphebet[$position] . '" class="letter-anchor"></a><hr>';
										$i++;
									} 
								} 
							?>
					<?php } ?>
					<?php if ( get_field('url') ) { ?>
						<a target="_blank" href="<?php echo the_field('url'); ?>">
							<h3><?php the_title(); ?></h3>
						</a>
					<?php } else { ?>
						<h3><?php the_title(); ?></h3>
					<?php } ?>
					<hr>
					<?php 
						if ( ($insurance_query->current_post + 1) == ($insurance_query->post_count) ) { 
							$i = 1; 
							$differance = 26 - $this_post_value;
							while( $i <= $differance ) {
								$position = $this_post_value + $i;
								echo '<h2>' . $alphebet[$position] . '</h2>';
								echo '<a id="' . $alphebet[$position] . '" class="letter-anchor"></a><hr>';
								$i++;
							} 
						}
					?>
					<?php $last_post_letter = $this_post_letter; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</section>
		<?php if( get_field('cta_button') ) { ?>
			<?php get_template_part('template-parts/elements/basic-cta'); ?>
		<?php } ?>
		<?php if( get_field('content_continued') ) { ?>
			<div class="default-contents">
				<?php the_field('content_continued'); ?>
			</div>
		<?php } ?>
	</article>
</main>

<?php get_footer(); ?>