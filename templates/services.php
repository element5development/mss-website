<?php 
/*-------------------------------------------------------------------
    Template Name: Services
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<main>
	<a id="content" class="anchor"></a>
	<article>
		<?php 
			$args = array(
				'post_type'      => 'page',
				'posts_per_page' => -1,
				'post_parent'    => $post->ID,
				'order'          => 'ASC',
				'orderby'        => 'menu_order'
		 );
		 $parent = new WP_Query( $args );
		?>
		<?php if ( $parent->have_posts() ) { ?>
			<div class="child-block">
				<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
					<?php get_template_part('template-parts/pages/child-preview'); ?>
				<?php endwhile; ?>
			</div>
		<?php } wp_reset_postdata(); ?>
		<?php if( !empty(get_the_content()) ) { ?>
			<div class="default-contents">
				<?php the_content(); ?>
			</div>
		<?php } ?>
		<?php if( get_field('cta_button') ) { ?>
			<?php get_template_part('template-parts/elements/basic-cta'); ?>
		<?php } ?>
		<?php if( get_field('content_continued') ) { ?>
			<div class="default-contents">
				<?php the_field('content_continued'); ?>
			</div>
		<?php } ?>
	</article>
</main>

<?php get_footer(); ?>