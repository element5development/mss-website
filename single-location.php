<?php 
/*-------------------------------------------------------------------

single location page template

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/posts/header-post'); ?>

	<main>
		<a id="content" class="anchor"></a>
		<div class="default-contents">
			<article>
				<section class="map">
					<div class="acf-map">
						<?php $location = get_field('map'); ?>
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							<h4><?php the_field('city'); ?></h4>
							<p class="address"><?php the_field('address'); ?> <?php the_field('address_line_2'); ?></p>
							<?php $mapLink = 'https://maps.google.com/?q=' . get_field('address') . get_field('address_line_2') . get_field('city') . get_field('state') . get_field('zip'); ?>
							<a target="_blank" href="<?php echo $mapLink ?>">view on google maps</a>
						</div>
					</div>
				</section>
				<section class="intro">
					<h2><?php the_field('city'); ?> <?php the_field('services'); ?></h2>
					<p><?php the_field('intro_paragraph'); ?></p>
					<a class="button is-secondary" href="<?php the_permalink('180'); ?>">Make An Appointment</a>
					<address>
						<?php the_field('address'); ?><br/>
						<?php the_field('address_line_2'); ?><br/>
						<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/><br/>
						Phone: <a href="tel:+1<?php echo $phone; ?>"><?php the_field('phone'); ?></a>
					</address>
				</section>
			</article>
			<?php if ( get_field('service_summary') ) : ?>
				<section class="services">
					<h2>Our Services<span>MSS <?php the_field('city'); ?> offers:</span></h2>
					<?php the_field('service_summary'); ?>
					<div class="cta-wrapper">
						<a class="button is-secondary" href="<?php the_permalink('180'); ?>">Make An Appointment</a>
					</div>
				</section>
			<?php endif; ?>
			<?php if( get_field('we_treat_left') & get_field('we_treat_right') ): ?>
				<section class="we-treat">
					<h2>What We Treat</h2>
					<div class="list">
						<?php the_field('we_treat_left'); ?>
					</div>
					<div class="list">
						<?php the_field('we_treat_right'); ?>
					</div>
				</section>
			<?php endif; ?>
			<div class="basic-cta" style="background-image: url('/wp-content/uploads/2018/01/lake.jpg');">
				<div class="block">
					<h2>Don’t suffer any longer.</h2>
					<p>Message us or call the appointment hotline today.</p>
					<a class="button is-secondary" href="<?php the_permalink('180'); ?>">Make An Appointment</a>
				</div>
				<div class="overlay"></div>
			</div>
			<section class="staff">
				<h2>MSS <?php the_field('city'); ?> Staff</span></h2>
				<?php 
					$terms = get_terms( array(
						'taxonomy' => 'position',
						'hide_empty' => false,
					) );
				?>
				<?php foreach ($terms as $term) { ?>
					<?php //QUERY STAFF
						$args = array( 
							'posts_per_page'  => -1, 
							'post_type' => 'staff',
							'orderby'=>'title',
							'order'=>'ASC',
							'tax_query' => array(
								array(
										'taxonomy' => 'position',
										'field' => 'slug',
										'terms' => array ( $term->slug )
								)
							),
							'meta_query' => array(
								array(
									'key' => 'office_locations', 
									'value' => '"' . get_the_ID() . '"',
									'compare' => 'LIKE'
								)
							)
						);
						$staff_query = new WP_Query( $args );
					?>
					<?php if ( $staff_query->have_posts() ) : ?>
						<div class="<?php echo  $term->slug; ?> staff-container">
							<h3><?php echo $term->name; ?></h3>
							<?php while ( $staff_query->have_posts() ) : $staff_query->the_post(); ?>
								<?php get_template_part('template-parts/posts/previews/preview-staff'); ?>
							<?php endwhile; ?>
						</div>
					<?php endif; wp_reset_postdata(); ?>
				<?php } ?>
			</section>
			<div class="basic-cta" style="background-image: url('/wp-content/uploads/2018/01/lake-forest.jpg');">
				<div class="block">
					<h2>Are you covered?</h2>
					<p>Check if your insurance will cover your procedure. We make it easy for you.</p>
					<a class="button is-secondary" href="/participating-insurance-companies/">Accepted Insurance</a>
				</div>
				<div class="overlay"></div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>