<?php 
/*-------------------------------------------------------------------
    Archive for Locations
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-archive'); ?>

<main>
	<a id="content" class="anchor"></a>
	<article>
			<div class="default-contents">
				<div class="acf-map">
					<?php while (have_posts()) : the_post(); ?>
						<?php $location = get_field('map'); ?>
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							<h4><?php the_field('city'); ?></h4>
							<p class="address"><?php the_field('address'); ?> <?php the_field('address_line_2'); ?></p>
							<?php $mapLink = 'https://maps.google.com/?q=' . get_field('address') . get_field('address_line_2') . get_field('city') . get_field('state') . get_field('zip'); ?>
							<a target="_blank" href="<?php echo $mapLink ?>">view on google maps</a>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="locations-container">
					<?php while (have_posts()) : the_post(); ?>
						<div class="single-location-preview">
							<a href="<?php the_permalink(); ?>">
								<h2><?php the_field('city'); ?></h2>
								<p><?php the_field('services'); ?></p>
							</a>
							<?php $phone = preg_replace( '/[^0-9]/', '', get_field('phone') ); ?>
							<address>
								<?php the_field('address'); ?><br/>
								<?php the_field('address_line_2'); ?><br/>
								<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/>
								Phone: <a href="tel:+1<?php echo $phone; ?>"><?php the_field('phone'); ?></a><br/>
							</address>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
	</article>
</main>

<?php get_footer(); ?>