var $ = jQuery;

$(document).ready(function () {
	/*------------------------------------------------------------------
  	ENTRANCE ANIMATIONS
	------------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*------------------------------------------------------------------
  	STICKY NAVIGATION
	------------------------------------------------------------------*/
	function defaultnav() {
		$('.nav-primary').removeClass('affixed').css('top', 'auto');
	}
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();

		if (scroll >= 300) {
			$('.nav-primary').addClass('affixed').css('top', '8px');
		} else if (scroll > 200 && scroll < 300) {
			$('.nav-primary').css('top', '-300px');
		} else {
			if (scroll < position) {
				$('.nav-primary').css('top', '-300px');
				setTimeout(defaultnav, 1000);
			}
			var position = scroll;
			$('.nav-primary').removeClass('affixed').css('top', 'auto');
		}
	});
	/*------------------------------------------------------------------
		FONTSIZE ADJUST
	------------------------------------------------------------------*/
	$(function () {
		var min = 16;
		var max = 20;
		if (Cookies.get('fontSize') >= 0) {
			fontSize = parseInt(Cookies.get('fontSize'));
			jQuery('body').css('font-size', fontSize);
		} else {
			fontSize = 16;
		}
		Cookies.set('fontSize', fontSize, {
			expires: 365,
			path: '/'
		});
		jQuery("#font-size").on('click', function (event) {
			event.preventDefault();
			if (fontSize < max) {
				fontSize = fontSize + 2;
			} else {
				fontSize = 16;
			}
			Cookies.set('fontSize', fontSize);
			jQuery('body').css('font-size', fontSize);
		});
		if (fontSize) {
			$('body').css('font-size', fontSize + 'px');
		}
	});
	/*------------------------------------------------------------------
  	HAMBURGER
	------------------------------------------------------------------*/
	$('.hamburger').on("click", function () {
		$(this).toggleClass('is-active');
		$('#menu-primary-navigation').toggleClass('is-active');
	});
	/*------------------------------------------------------------------
		INPUT ADDING AND REMVOING CLASSES
	------------------------------------------------------------------*/
	$('input:not([type=checkbox]):not([type=radio])').on("focus", function () {
		$(this).addClass('is-activated');
	}).blur(function () {
		if ($(this).val().length > 0) {
			// nothing
		} else {
			$(this).removeClass('is-activated');
		}
	});
	$('textarea').focus(function () {
		$(this).addClass('is-activated');
	}).blur(function () {
		if ($(this).val().length > 0) {
			// nothing
		} else {
			$(this).removeClass('is-activated');
		}
	});
	$('select').focus(function () {
		$(this).addClass('is-activated');
	}).blur(function () {
		if ($(this).val().length > 0) {
			// nothing
		} else {
			$(this).removeClass('is-activated');
		}
	});
	/*------------------------------------------------------------------
  	LABEL ANIMATION
	------------------------------------------------------------------*/
	$('input').on("focus", function () {
		$(this).parents('li').addClass('selected');
	}).blur(function () {
		if ($(this).val().length > 0) {
			// nothing
		} else {
			$(this).parents('li').removeClass('selected');
		}
	});
	$('textarea').on("focus", function () {
		$(this).parents('li').addClass('selected');
	}).blur(function () {
		if ($(this).val().length > 0) {
			// nothing
		} else {
			$(this).parents('li').removeClass('selected');
		}
	});
	/*------------------------------------------------------------------
  	SEARCH FORM LOGIC
	------------------------------------------------------------------*/
	$('#search-form input').on("focus", function () {
		$(this).siblings('label').addClass('selected');
	}).blur(function () {
		if ($(this).val().length > 0) {
			// nothing
		} else {
			$(this).siblings('label').removeClass('selected');
		}
	});
	$('#search-open').on("click", function () {
		$(this).addClass('hidden');
		$('#search-form').removeClass('hidden');
	});
	$('#mobile-search').on("click", function () {
		$(this).toggleClass('is-active');
		$('.mobile-search-form').toggleClass('is-active');
	});
	/*------------------------------------------------------------------
  	LOGIC FOR LOAD MORE BUTTON
	------------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'block');
	}
	/*------------------------------------------------------------------
		INFINITE SCROLL INIT
	------------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.post-preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*------------------------------------------------------------------
		AWARDS SLIDER
	------------------------------------------------------------------*/
	$('.awards').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		prevArrow: '<svg class="slick-prev" width="44" height="44" viewBox="0 0 44 44" xmlns="http://www.w3.org/2000/svg"><g transform="matrix(0 -1 -1 0 41.685 41.89)" fill="none" fill-rule="evenodd"><path d="M19.253 22.278l-6.97-6.969a1.312 1.312 0 0 0-1.856 1.857l7.878 7.878c.239.247.574.401.945.401.377 0 .712-.154.95-.402l7.879-7.877c.223-.24.362-.559.362-.912a1.313 1.313 0 0 0-2.31-.855l-6.878 6.88z" fill="#00B4EA"/><circle stroke="#00B4EA" stroke-width="4" transform="matrix(-1 0 0 1 39.117 0)" cx="19.559" cy="19.441" r="19.441"/></g></svg>',
		nextArrow: '<svg class="slick-next" width="44" height="44" viewBox="0 0 44 44" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path id="a" d="M0 0h18.382v10.504H0z"/></defs><g transform="rotate(-90 22.05 19.841)" fill="none" fill-rule="evenodd"><g transform="translate(10.059 14.941)"><mask id="b" fill="#fff"><use xlink:href="#a"/></mask><path d="M9.188 7.337l6.97-6.97a1.312 1.312 0 0 1 1.855 1.857l-7.877 7.879a1.31 1.31 0 0 1-.945.4 1.31 1.31 0 0 1-.95-.4L.361 2.224A1.333 1.333 0 0 1 0 1.313 1.313 1.313 0 0 1 2.31.458l6.879 6.879z" fill="#00B4EA" mask="url(#b)"/></g><circle stroke="#00B4EA" stroke-width="4" cx="19.559" cy="19.441" r="19.441"/></g></svg>',
		responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				}
			},
		]
	});
	/*------------------------------------------------------------------
  	LOCATION MAPS
	------------------------------------------------------------------*/
	$('.acf-map').each(function () {
		map = new_map($(this));
	});

	function new_map($el) {
		var $markers = $el.find('.marker');
		var args = {
			zoom: 16,
			center: new google.maps.LatLng(0, 0),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map($el[0], args);
		map.markers = [];
		$markers.each(function () {
			add_marker($(this), map);
		});
		center_map(map);
		return map;
	}

	function add_marker($marker, map) {
		var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
		var marker = new google.maps.Marker({
			position: latlng,
			map: map
		});
		map.markers.push(marker);
		if ($marker.html()) {
			var infowindow = new google.maps.InfoWindow({
				content: $marker.html()
			});
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.open(map, marker);
			});
		}
	}

	function center_map(map) {
		var bounds = new google.maps.LatLngBounds();
		$.each(map.markers, function (i, marker) {
			var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
			bounds.extend(latlng);
		});
		if (map.markers.length == 1) {
			map.setCenter(bounds.getCenter());
			map.setZoom(16);
		} else {
			map.fitBounds(bounds);
		}
	}
	/*------------------------------------------------------------------
  	STAFF FILTER
	------------------------------------------------------------------*/
	$('.cat-nav').on('click', 'button', function () {
		$('.cat-nav button').removeClass('is-active');
		$(this).addClass('is-active');
		var filterValue = $(this).attr('data-filter');
		console.log(filterValue);
		if (filterValue == 'all') {
			$('.staff-container').removeClass('hide');
		} else if ($('.staff-container').hasClass(filterValue)) {
			$('.staff-container').addClass('hide');
			$('.' + filterValue).removeClass('hide');
		}
	});
	/*------------------------------------------------------------------
  	ACCORDION
	------------------------------------------------------------------*/
	$('.accordion-head').on('click', function () {
		$(this).toggleClass('open');
		$('.accordion-content').toggleClass('open');
	});
	/*------------------------------------------------------------------
  	STICKY ASIDE
	------------------------------------------------------------------*/
	if ($('body').hasClass('single-post')) {
		var sidebar = new StickySidebar('aside', {
			containerSelector: '.default-contents',
			innerWrapperSelector: '.sidebar__inner',
			topSpacing: 135,
			bottomSpacing: 40,
			minWidth: 1024
		});
	}
	if ($('body').hasClass('blog') || $('body').hasClass('search')) {
		var sidebar = new StickySidebar('aside', {
			containerSelector: '.default-contents',
			innerWrapperSelector: '.sidebar__inner',
			topSpacing: 135,
			bottomSpacing: 40,
			minWidth: 1024
		});
	}
});