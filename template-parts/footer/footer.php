<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>

<?php if ( !is_page(163) ) { ?>
	<?php get_template_part('template-parts/elements/meet-staff'); ?>
<?php } ?>

<footer class="site-footer">
	<div class="block">
		<div class="block">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/mss-white.png" alt="Michigan Surgery Specialists" />
			<address>
				<h5>Headquarters</h5>
				<a target="_blank" href="https://maps.google.com/?q=<?php the_field('primary_address','option'); ?>, <?php the_field('primary_city','option'); ?>, <?php the_field('primary_state','option'); ?> <?php the_field('primary_zip','option'); ?>">
					<?php the_field('primary_address','option'); ?><br/>
					<?php the_field('primary_city','option'); ?>, <?php the_field('primary_state','option'); ?> <?php the_field('primary_zip','option'); ?>
				</a>
				<?php $phone = preg_replace( '/[^0-9]/', '', get_field('primary_phone', 'option') ); ?>
				<p><span>Office: </span><a href="tel:+1<?php echo $phone; ?>"><?php the_field('primary_phone', 'option'); ?></a></p>
				<?php $appointments = preg_replace( '/[^0-9]/', '', get_field('appointments_phone', 'option') ); ?>
				<p><span>Appointments: </span><a href="tel:+1<?php echo $appointments; ?>"><?php the_field('appointments_phone', 'option'); ?></a></p>
			</address>
		</div>
		<div class="block">
			<?php if (has_nav_menu('patient_nav')) : ?>
				<h5>Patient information</h5>
				<?php wp_nav_menu(['theme_location' => 'patient_nav']); ?>
			<?php endif; ?>
			<?php if (has_nav_menu('professionals_nav')) : ?>
				<h5>For healthcare professionals</h5>
				<?php wp_nav_menu(['theme_location' => 'professionals_nav']); ?>
			<?php endif; ?>
		</div>
		<div class="block">
			<h5>Connect with us</h5>
			<?php get_template_part('template-parts/navigation/social'); ?>
			<?php if (has_nav_menu('specialists_nav')) : ?>
				<h5>Michigan Surgery Specialists, P.C.</h5>
				<?php wp_nav_menu(['theme_location' => 'specialists_nav']); ?>
			<?php endif; ?>
		</div>
		<section data-emergence="hidden"  id="element5-credit">
			<a target="_blank" href="https://element5digital.com">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" />
			</a>
		</section>
	</div>
</footer>
<section class="copyright">
	<div class="block">
		<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		<?php if (has_nav_menu('footer_nav')) : ?>
			<?php wp_nav_menu(['theme_location' => 'footer_nav']); ?>
		<?php endif; ?>
	</div>	
</section>