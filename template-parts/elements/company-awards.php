

	<?php if( have_rows('awards', 'options') ): ?>
		<div class="awards">
			<?php while ( have_rows('awards', 'options') ) : the_row(); ?>
				<div class="single-award">
				<?php
					$award = get_sub_field('icon');
					$alt = $award['alt'];
					$small = $award['sizes']['small'];
				?>
				<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" />
				<p>
					<?php the_sub_field('title'); ?><br/>
					<b><?php the_sub_field('year'); ?></b>
				</p>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>