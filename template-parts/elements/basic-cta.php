<?php 
	if ( get_field('cta_background') ) { 
		$background = get_field('cta_background'); 
		$background_url = $background['url'];
	} else {
		$background_url = get_stylesheet_directory_uri() . '/dist/images/cta-background.jpg';
	}
?>

<div class="basic-cta" style="background-image: url('<?php echo $background_url; ?>');">
	<div class="block">
		<?php if ( get_field('cta_heading') ) { ?>
			<h2><?php the_field('cta_heading'); ?></h2>
		<?php } ?>
		<?php if ( get_field('cta_description') ) { ?>
			<p><?php the_field('cta_description'); ?></p>
		<?php } ?>
		<?php $button = get_field('cta_button'); ?>
		<a class="button is-secondary" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
	</div>
	<div class="overlay"></div>
</div>