<section class="meet-staff">
	<h2>Meet our physicians</h2>
	<?php //QUERY STAFF
		$args = array( 
			'posts_per_page'  => 3, 
			'post_type' => 'staff',
			'orderby'=>'rand',
			'tax_query' => array(
				array(
						'taxonomy' => 'position',
						'field' => 'slug',
						'terms' => array ( 'hand-surgery', 'orthopedics' )
				)
			)
		);
		$staff_query = new WP_Query( $args );
	?>
	<?php if ( $staff_query->have_posts() ) : ?>
		<?php while ( $staff_query->have_posts() ) : $staff_query->the_post(); ?>
			<?php //LOGIC TO DETERMINE FEATURED IMAGE
				$image = get_field('headshot');
				$alt = $image['alt'];
				$image_thumb = $image['sizes']['thumbnail'];
			?>
			<article>
				<img src="<?php echo $image_thumb; ?>" alt="<?php echo $alt; ?>" />
				<?php if ( get_field('post_title') ) { ?>
					<h3><?php the_field('post_title'); ?></h3>
				<?php } else { ?>
					<h3><?php the_title(); ?></h3>
				<?php } ?>
				<h4>
					<?php 
						$terms = get_the_term_list( $post->ID, 'position', ' ',', ' ); 
						$terms = strip_tags( $terms );
						echo $terms;
					?>
				</h4>
				<?php if ( get_field('last_name') ) { ?>
					<a class="button is-secondary" href="<?php the_permalink(); ?>">Meet Dr. <?php the_field('last_name'); ?></a>
				<?php } else { ?>
					<a class="button is-secondary" href="<?php the_permalink(); ?>">Learn More</a>
				<?php } ?>
			</article>
		<?php endwhile; ?>
	<?php endif; ?>
</section>