<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<header class="page-title">
	<div class="block">
		<?php if ( get_field('page_title') ) { ?>
			<h1><?php the_field('page_title'); ?></h1>
		<?php } else { ?>
			<h1><?php the_title(); ?></h1>
		<?php } ?>
		<?php if ( get_field('page_description') ) { ?>
			<p><?php the_field('page_description'); ?></p>
		<?php } ?>
		<?php if ( get_field('page_cta') ) { ?>
			<?php $link = get_field('page_cta'); ?>
			<a class="button is-secondary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php } ?>
	</div>
</header>