<div class="page-intro default-contents">
	<?php 
		$image = get_field('intro_image');
		$size = 'large';
		$large = $image['sizes'][ $size ];
	?>
	<?php if ( is_page(174) ) { ?>
		<a target="_blank" href="https://motusheals.com/"><img src="<?php echo $large; ?>" alt="<?php echo $image['alt']; ?>" /></a>
	<?php } else { ?>
		<img src="<?php echo $large; ?>" alt="<?php echo $image['alt']; ?>" />
	<?php } ?>
	<?php the_field('intro_content'); ?>
</div>