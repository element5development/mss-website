<article class="child-page-prview">
	<?php 
		$image = get_field('intro_image');
		$size = 'large';
		$large = $image['sizes'][ $size ];
	?>
	<img src="<?php echo $large; ?>" alt="<?php echo $image['alt']; ?>" />
	<?php if ( get_field('page_title') ) { ?>
		<h2><?php the_field('page_title'); ?></h2>
	<?php } else { ?>
		<h2><?php the_title(); ?></h2>
	<?php } ?>
	<?php the_excerpt(); ?>
	<a class="button is-secondary" href="<?php the_permalink(); ?>">Learn More</a>
</article>