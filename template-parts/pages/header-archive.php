<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<header class="archive-title page-title">
	<div class="block">
		<?php if ( is_search() ) { ?>
			<h1>Search: <?php the_search_query(); ?></h1>
		<?php } else if ( is_home() )  { ?>
			<h1>News</h1>
		<?php } else if ( is_archive() )  { ?>
			<h1><?php echo post_type_archive_title( '', false)  ?></h1>
			<?php if( is_post_type_archive('location') ) : ?>
				<p>Convenient Locations in Southeast Michigan</p>
			<?php endif; ?>
		<?php } else { } ?>
	</div>
</header>