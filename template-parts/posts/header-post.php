<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<?php //LOGIC TO DETERMINE FEATURED IMAGE
	if ( has_post_thumbnail() ) {
		$featured_image_url = get_the_post_thumbnail_url();
	} else {
		$featured_image_url =  get_stylesheet_directory_uri() . '/dist/images/title-background.jpg';
	}
?>

<header class="post-title" style="background-image: url('<?php echo $featured_image_url; ?>');">
	<div class="block">
		<?php if ( get_field('post_title') ) { ?>
			<h1><?php the_field('post_title'); ?></h1>
		<?php } else { ?>
			<h1><?php the_title(); ?></h1>
		<?php } ?>
		<?php if ( get_field('post_description') ) { ?>
			<p><?php the_field('post_description'); ?></p>
		<?php } ?>
		<?php if ( get_field('post_cta') ) { ?>
			<?php $link = get_field('post_cta'); ?>
			<a class="button is-secondary" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php } ?>
	</div>
</header>