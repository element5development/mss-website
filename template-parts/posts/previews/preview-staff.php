<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //LOGIC TO DETERMINE FEATURED IMAGE
	$image = get_field('headshot');
	$alt = $image['alt'];
	$image_thumb = $image['sizes']['thumbnail'];
?>

<?php if( has_term( 'staff', 'position' ) ) { ?>

	<?php if ( get_field('email') ) { ?>
		<article class="staff-preview">
			<a href="mailto:<?php the_field('email'); ?>">
				<img src="<?php echo $image_thumb; ?>" alt="<?php echo $alt; ?>" />
			</a>
			<header>
				<a href="mailto:<?php the_field('email'); ?>">
					<?php if ( get_field('post_title') ) { ?>
						<h3>
							<?php the_field('post_title'); ?>
						</h3>
					<?php } else { ?>
						<h3>
							<?php the_title(); ?>
						</h3>
					<?php } ?>
					<svg width="55" height="54" viewBox="0 0 55 54">
						<defs>
							<path id="ba" d="M331.29 987a27 27 0 1 1 54 0 27 27 0 0 1-54 0z"/>
							<path id="aa" d="M346 978.43h24.1V995H346z"/>
							<path id="da" d="M358.05 990.1l3.07-2.96 8.63 7.55c-.2.2-.48.31-.78.31h-21.84c-.3 0-.57-.12-.77-.31l8.63-7.55zm-11.75 4.54c-.18-.2-.3-.47-.3-.77v-14.31c0-.31.13-.6.34-.8l7.33 7.1zm.08-15.92c.2-.18.46-.29.75-.29h21.84c.3 0 .56.11.76.3l-11.68 9.11zm16.05 7.14l7.34-7.1c.2.2.33.49.33.8v14.31c0 .3-.11.57-.3.77z"/>
							<clipPath id="ca">
								<use xlink:href="#aa"/>
							</clipPath>
						</defs>
						<use fill="#2956a9" xlink:href="#ba" transform="translate(-331 -960)"/>
						<g clip-path="url(#ca)" transform="translate(-331 -960)">
							<use fill="#fff" xlink:href="#da"/>
						</g>
					</svg>
				</a>
			</header>
			<div class="title">
				<?php if ( get_field('post_description') ) { ?>
					<p><?php the_field('post_description'); ?></p>
				<?php } ?>
			</div>
		</article>
	<?php } else { ?>
		<article class="staff-preview">
			<img src="<?php echo $image_thumb; ?>" alt="<?php echo $alt; ?>" />
			<header>
				<?php if ( get_field('post_title') ) { ?>
					<h3><?php the_field('post_title'); ?></h3>
				<?php } else { ?>
					<h3><?php the_title(); ?></h3>
				<?php } ?>
			</header>
			<div class="title">
				<?php if ( get_field('post_description') ) { ?>
					<p><?php the_field('post_description'); ?></p>
				<?php } ?>
			</div>
		</article>
	<?php } ?>

<?php } else { ?>

	<article class="staff-preview doctor">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php echo $image_thumb; ?>" alt="<?php echo $alt; ?>" />
		</a>
		<header>
			<a href="<?php the_permalink(); ?>">
				<?php if ( get_field('post_title') ) { ?>
					<h3><?php the_field('post_title'); ?></h3>
				<?php } else { ?>
					<h3><?php the_title(); ?></h3>
				<?php } ?>
			</a>
		</header>
		<div class="location">
			<p>
				<b>Location(s):</b>
				<?php $posts = get_field('office_locations'); ?>
				<?php if( $posts ): ?>
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						<?php setup_postdata($post); ?>
						<span><?php the_field('city'); ?></span>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>
			</p>
		</div>
		<div class="title">
			<?php if ( get_field('post_description') ) { ?>
				<p><?php the_field('post_description'); ?></p>
			<?php } ?>
		</div>
	</article>

<?php } ?>