<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //LOGIC TO DETERMINE FEATURED IMAGE
if ( has_post_thumbnail() ) {
	$medium_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');
  $domain = get_site_url(); // returns something like http://domain.com
  $relative_url = str_replace( $domain, '', $medium_image_url[0] );
  $featured_image_url = $relative_url;
} else {
	$featured_image_url = get_stylesheet_directory_uri() . '/dist/images/post-default.jpg';
}
?>

<div class="post-preview">
	<article>
		<a href="<?php the_permalink(); ?>">
			<header>
				<div class="featured-image" style="background-image: url('<?php echo $featured_image_url; ?>');"></div>
				<?php if ( get_field('post_title') ) { ?>
					<h1><?php the_field('post_title'); ?></h1>
				<?php } else { ?>
					<h1><?php the_title(); ?></h1>
				<?php } ?>
			</header>
			<blockquote>
				<?php if ( get_field('post_description') ) { ?>
					<p><?php the_field('post_description'); ?></p>
				<?php } else { ?>
					<?php the_excerpt(); ?>
				<?php } ?>
			</blockquote>
		</a>
	</article>
</div>