<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<div class="post-preview">
	<article>
		<a href="<?php the_permalink(); ?>">
			<header>
				<?php if ( get_field('post_title') ) { ?>
					<h1><?php the_field('post_title'); ?></h1>
				<?php } else if ( get_field('page_title') ) { ?>
					<h1><?php the_field('page_title'); ?></h1>
				<?php } else { ?>
					<h1><?php the_title(); ?></h1>
				<?php } ?>
			</header>
			<blockquote>
				<?php if ( get_the_excerpt() ) { ?>
					<?php the_excerpt(); ?>
				<?php } else { ?>
					<p><?php the_field('post_description'); ?></p>
				<?php } ?>
			</blockquote>
		</a>
	</article>
</div>