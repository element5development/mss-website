<nav class="social-nav">
	<a target="_blank" href="https://www.facebook.com/MichiganSurgerySpecialistsPC/" class="social-button facebook">
	<svg width="55" height="55" viewBox="0 0 55 55">
		<defs>
			<path id="facebook" d="M0 0h12.208v26.034H0z"/>
		</defs>
		<g transform="translate(1.5 1)" fill="none" fill-rule="evenodd">
			<circle fill="#2956A9" cx="26" cy="26.272" r="26"/>
			<g transform="translate(20 13.577)">
				<mask id="facebook2" fill="#fff">
					<use xlink:href="#facebook"/>
				</mask>
				<path d="M12.182.008v4.19s-3.088-.309-3.861.875c-.423.647-.172 2.54-.21 3.9h4.097c-.347 1.591-.595 2.669-.85 4.046h-3.27v13.015H2.416V13.07H0V8.974h2.388c.122-2.995.168-5.961 1.656-7.472C5.716-.196 7.31.008 12.182.008" fill="#FFF" mask="url(#facebook2)"/>
			</g>
		</g>
	</svg>
	</a>
	<a target="_blank" href="https://www.linkedin.com/company/529395/" class="social-button linkedin">
		<svg width="55" height="55" viewBox="0 0 55 55">
			<g transform="translate(1.5 1.5)" fill="none" fill-rule="evenodd">
				<circle fill="#2956A9" cx="26" cy="26" r="26"/>
				<path d="M16.001 35.393h4.88V21.546H16v13.847zm2.45-15.9c-1.504 0-2.722-1.092-2.722-2.441 0-1.35 1.218-2.445 2.721-2.445 1.502 0 2.72 1.094 2.72 2.445 0 1.349-1.218 2.441-2.72 2.441zm14.237 15.9V27.83c0-1.55-.449-3.104-2.275-3.104-1.825 0-2.59 1.554-2.59 3.141v7.525h-4.87V21.546h4.87v1.865c1.283-1.63 2.397-2.301 4.416-2.301 2.021 0 5.307.943 5.307 6.411v7.872h-4.858z" fill="#FFF"/>
			</g>
		</svg>
	</a>
	<a target="_blank" href="https://www.youtube.com/user/ORH2006" class="social-button youtube">
		<svg width="55" height="55" viewBox="0 0 55 55">
			<g transform="translate(1.5 1.5)" fill="none" fill-rule="evenodd">
				<circle fill="#2956A9" cx="26" cy="26" r="26"/>
				<path d="M24.419 29.813l-.048-7.54 5.248 3.802-5.2 3.738zm9.834-12.034l-7.817-.006-8.122.006C16.28 17.78 14 19.136 14 21.125v9.901c0 1.989 2.281 3.201 4.314 3.201h15.939c2.032 0 3.679-1.212 3.679-3.2v-9.902c0-1.99-1.647-3.346-3.679-3.346z" fill="#FFF"/>
			</g>
		</svg>
	</a>
</nav>