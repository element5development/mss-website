<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<a href="#content" class="skip-nav button smooth-scroll">Skip to Content</a>

<nav class="nav-primary">
	<div class="nav_inner">
		<div class="top-nav">
			<a target="_blank" href="https://portal.allmeds.com/memManagement/LoginAccount.aspx?location=R665">My Patient Portal</a>
			<a href="javascript:void(0)" id="font-size">
				<svg viewBox="0 0 250 250" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
					<path d="M36.585 117.891h24.39v113.751h-24.39z"/>
					<path d="M0 110.78h97.561v28.438H0zM152.439 25.467h24.39v206.174h-24.39z"/>
					<path d="M79.268 18.358H250v28.438H79.268z"/>
				</svg>
			</a>
			<?php $phone = preg_replace( '/[^0-9]/', '', get_field('primary_phone', 'option') ); ?>
			<a href="tel:+1<?php echo $phone; ?>" class="phone">
				<svg viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
					<path data-name="layer1" d="M48.5 40.2a4.8 4.8 0 0 0-6.5 1.3c-2.4 2.9-5.3 7.7-16.2-3.2S19.6 24.4 22.5 22a4.8 4.8 0 0 0 1.3-6.5L17 5.1c-.9-1.3-2.1-3.4-4.9-3S2 6.6 2 15.6s7.1 20 16.8 29.7S39.5 62 48.4 62s13.2-8 13.5-10-1.7-4-3-4.9z" />
				</svg>
			</a>
			<a href="javascript:void(0)" class="search-block">
				<div id="search-form" class="hidden">
					<?php echo get_search_form(); ?>
				</div>
				<svg id="search-open" viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
					<path data-name="layer1" d="M58.121 52.879L42.545 37.302l-.29.308a20.009 20.009 0 1 0-3.777 3.995l-.057.06L53.88 57.12a3 3 0 0 0 4.242-4.242zM26 41a15 15 0 1 1 15-15 15.017 15.017 0 0 1-15 15z" />
				</svg>
			</a>
		</div>
		<a class="brand" href="<?php echo get_site_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/mss-white.png" alt="Michigan Surgery Specialists" />
		</a>
		<div class="mobile-nav">
			<a href="javascript:void(0)" class="search-mobile">
				<svg id="mobile-search" viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
					<path data-name="layer1" d="M58.121 52.879L42.545 37.302l-.29.308a20.009 20.009 0 1 0-3.777 3.995l-.057.06L53.88 57.12a3 3 0 0 0 4.242-4.242zM26 41a15 15 0 1 1 15-15 15.017 15.017 0 0 1-15 15z" />
				</svg>
				<div id="search-form" class="mobile-search-form">
					<?php echo get_search_form(); ?>
				</div>
			</a>
			<?php $phone = preg_replace( '/[^0-9]/', '', get_field('primary_phone', 'option') ); ?>
			<a href="tel:+1<?php echo $phone; ?>" class="phone-mobile">
				<svg viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
					<path data-name="layer1" d="M48.5 40.2a4.8 4.8 0 0 0-6.5 1.3c-2.4 2.9-5.3 7.7-16.2-3.2S19.6 24.4 22.5 22a4.8 4.8 0 0 0 1.3-6.5L17 5.1c-.9-1.3-2.1-3.4-4.9-3S2 6.6 2 15.6s7.1 20 16.8 29.7S39.5 62 48.4 62s13.2-8 13.5-10-1.7-4-3-4.9z" />
				</svg>
			</a>
			<button class="hamburger hamburger--collapse" type="button">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
		</div>
		<?php if (has_nav_menu('primary_nav')) :
			wp_nav_menu(['theme_location' => 'primary_nav']);
		endif; ?>
	</div>
</nav>