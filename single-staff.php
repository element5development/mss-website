<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php //LOGIC TO DETERMINE FEATURED IMAGE
	$image = get_field('headshot');
	$alt = $image['alt'];
	$image_med = $image['sizes']['medium'];
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/posts/header-post'); ?>

	<main>
		<a id="content" class="anchor"></a>
		<div class="default-contents">
			<aside>
				<div class="staff-headshot">
					<img src="<?php echo $image_med; ?>" alt="<?php echo $alt; ?>" />
					<?php if ( get_field('post_title') ) { ?>
						<h3><?php the_field('post_title'); ?></h3>
					<?php } else { ?>
						<h3><?php the_title(); ?></h3>
					<?php } ?>
					<?php if ( get_field('phone') || get_field('email') || get_field('linkedin') ) { ?>
						<nav class="contact">
							<?php if ( get_field('phone') ) { ?>
								<?php $phone = preg_replace( '/[^0-9]/', '', get_field('phone') ); ?>
								<a href="+1<?php echo $phone; ?>">
									<svg width="54" height="54" viewBox="0 0 54 54">
										<defs>
											<path id="b" d="M251 987a27 27 0 1 1 54 0 27 27 0 0 1-54 0z"/>
											<path id="a" d="M267 974.73h22.68v22.68H267z"/>
											<path id="d" d="M284.58 989.18a1.83 1.83 0 0 0-2.47.48c-.89 1.1-1.98 2.93-6.14-1.22-4.15-4.16-2.32-5.25-1.22-6.14 1.1-.89.78-2.01.48-2.47-.3-.46-2.23-3.43-2.57-3.93-.34-.5-.8-1.3-1.87-1.15a5.42 5.42 0 0 0-3.79 5.1c0 3.4 2.68 7.57 6.33 11.23 3.66 3.65 7.83 6.33 11.22 6.33 3.4 0 5-3 5.1-3.8.16-1.06-.64-1.52-1.14-1.86-.5-.34-3.47-2.27-3.93-2.57"/>
											<clipPath id="c">
												<use xlink:href="#a"/>
											</clipPath>
										</defs>
										<use fill="#2956a9" xlink:href="#b" transform="translate(-251 -960)"/>
										<g clip-path="url(#c)" transform="translate(-251 -960)">
											<use fill="#fff" xlink:href="#d"/>
										</g>
									</svg>
								</a>
							<?php } ?>
							<?php if ( get_field('email') ) { ?>
								<a href="mailto:<?php echo get_field('email'); ?>">
									<svg width="55" height="54" viewBox="0 0 55 54">
										<defs>
											<path id="ba" d="M331.29 987a27 27 0 1 1 54 0 27 27 0 0 1-54 0z"/>
											<path id="aa" d="M346 978.43h24.1V995H346z"/>
											<path id="da" d="M358.05 990.1l3.07-2.96 8.63 7.55c-.2.2-.48.31-.78.31h-21.84c-.3 0-.57-.12-.77-.31l8.63-7.55zm-11.75 4.54c-.18-.2-.3-.47-.3-.77v-14.31c0-.31.13-.6.34-.8l7.33 7.1zm.08-15.92c.2-.18.46-.29.75-.29h21.84c.3 0 .56.11.76.3l-11.68 9.11zm16.05 7.14l7.34-7.1c.2.2.33.49.33.8v14.31c0 .3-.11.57-.3.77z"/>
											<clipPath id="ca">
												<use xlink:href="#aa"/>
											</clipPath>
										</defs>
										<use fill="#2956a9" xlink:href="#ba" transform="translate(-331 -960)"/>
										<g clip-path="url(#ca)" transform="translate(-331 -960)">
											<use fill="#fff" xlink:href="#da"/>
										</g>
									</svg>
								</a>
							<?php } ?>
							<?php if ( get_field('linkedin') ) { ?>
								<a target="_blank" href="<?php echo get_field('linkedin'); ?>">
									<svg width="55" height="54" viewBox="0 0 55 54">
										<defs>
											<path id="ab" d="M411.58 987a27 27 0 1 1 54 0 27 27 0 0 1-54 0z"/>
											<path id="bb" d="M428.27 996.4h4.88v-13.85h-4.88zm2.45-15.9c1.5 0 2.72-1.1 2.72-2.45s-1.22-2.44-2.72-2.44c-1.5 0-2.72 1.1-2.72 2.44 0 1.35 1.22 2.45 2.72 2.45zm19.1 15.9v-7.88c0-5.46-3.29-6.4-5.31-6.4-2.02 0-3.13.66-4.42 2.3v-1.87h-4.87v13.85h4.87v-7.53c0-1.59.77-3.14 2.6-3.14 1.82 0 2.27 1.55 2.27 3.1v7.57z"/>
										</defs>
										<use fill="#2956a9" xlink:href="#ab" transform="translate(-411 -960)"/>
										<use fill="#fff" xlink:href="#bb" transform="translate(-411 -960)"/>
									</svg>
								</a>
							<?php } ?>
						</nav>
					<?php } ?>
				</div>
				<div class="stafff-location">
					<?php $posts = get_field('office_locations'); ?>
					<?php if( $posts ): ?>
						<?php foreach( $posts as $post): ?>
							<?php setup_postdata($post); ?>
							<h4><?php the_field('city'); ?> Location</h4>
							<?php $location = get_field('map'); ?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
							<p>
								<?php the_field('address'); ?><br/>
								<?php the_field('address_line_2'); ?><br/>
								<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/>
							</p>
							<?php wp_reset_postdata(); ?>
						<?php endforeach; ?>
					<?php endif; ?>
					<a href="/contact-us/" class="button is-secondary">Make Appointment</a>
				</div>
			</aside>
			<article>
				<?php the_field('biography'); ?>
				<!-- <h3>Specialty</h3>
				<p>
					<?php 
						$terms = wp_get_post_terms($post->ID, 'specialty');
						$count = count($terms);
						if ( $count > 0 ) {
							foreach ( $terms as $term ) {
								echo '<span class="specialty">' . $term->name . '</span>';
							}
						}
					?>
				</p> -->
				<?php the_field('education'); ?>
				<?php if( have_rows('awards') ): ?>
					<h2>Awards</h2>
					<div class="award-block">
						<?php while ( have_rows('awards') ) : the_row(); ?>
							<div class="award">
							<?php
								$award = get_sub_field('icon');
								$alt = $award['alt'];
								$small = $award['sizes']['small'];
							?>
							<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" />
							<p>
								<?php the_sub_field('title'); ?><br/>
								<b><?php the_sub_field('year'); ?></b>
							</p>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<?php $posts = get_field('participating_insurances'); ?>
				<?php if( $posts ): ?>
					<h2 class="accordion-head">Participating Insurances</h2>
					<ul class="accordion-content">
						<?php foreach( $posts as $post): ?>
							<?php setup_postdata($post); ?>
							<li><?php the_title(); ?></li>
							<?php wp_reset_postdata(); ?>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</article>
		</div>
	</main>

<?php get_footer(); ?>