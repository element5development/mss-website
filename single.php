<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/posts/header-post'); ?>

	<main>
		<a id="content" class="anchor"></a>
		<div class="default-contents">
			<article>
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			</article>
			<aside>
				<?php dynamic_sidebar( 'sidebar-blog' ); ?>
			</aside>
		</div>
		<div class="default-contents">
			<?php if ( comments_open() || get_comments_number() ) : ?>
				<?php comments_template(); ?>
			<?php endif; ?>
		</div>
	</main>

<?php get_footer(); ?>